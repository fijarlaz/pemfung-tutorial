> data Buyer
> = Family Int Int Int -- Keluarga dihitung jumlah dewasa, abg, balita
> | Single -- beli satuan tiket
> | WithFriends Int -- jumlah group
> | Member Buyer -- ada kartu member
> | Company Int -- Jumlah karyawan yang beli tiket
> deriving (Show)

> hargaTiket satuan (Family x y z) = x*satuan + y*satuan - 100 -- balita gratis
> hargaTiket satuan (Single) = satuan
> hargaTiket satuan (WithFriends n) = satuan*n - 50
> hargaTiket satuan (Member b) = (hargaTiket satuan b) - 200
> hargaTiket satuan (Company n) = n * satuan - 150

> hitungPenonton (Family x y z) = x+y+z
> hitungPenonton (Single) = 1
> hitungPenonton (WithFriends n) = n
> hitungPenonton (Member b) = hitungPenonton b
> hitungPenonton (Company n) = n

> foldBuyer (f,s,w,m,c) (Family x y z) = f x y z
> foldBuyer (f,s,w,m,c) (Single) = s
> foldBuyer (f,s,w,m,c) (WithFriends n) = w n
> foldBuyer (f,s,w,m,c) (Member b) = m (foldBuyer (f,s,w,m,c) b)
> foldBuyer (f,s,w,m,c) (Company n) = c n

> newHargaTiket satuan buyer = foldBuyer (tf,ts,tw,tm,tc) buyer
> where tf x y z = satuan*x + satuan*y - 100 -- balita gratis
> ts = satuan
> tw n = satuan * n - 50
> tm x = x - 200
> tc n = satuan * n - 150

> newHitungPenonton buyer = foldBuyer (tf,ts,tw,tm,tc) buyer
> where tf x y z = x + y + z
> ts = 1
> tw n = n
> tm m = m
> tc n = n

> buyers = [Family 3 4 2, Single, WithFriends 10, Member Single, Company 50]

-- >>> map (hargaTiket 10000) buyers
-- >>> map (newHargaTiket 10000) buyers
-- >>> map hitungPenonton buyers
-- >>> map newHitungPenonton buyers
-- [69900,10000,99950,9800,499850]
-- [69900,10000,99950,9800,499850]
-- [9,1,10,1,50]
-- [9,1,10,1,50]