iter n f x
    | n==0 = x
    | otherwise = f (iter (n-1)f x)