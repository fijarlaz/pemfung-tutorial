maxTiga :: (Ord a) => a -> a -> a -> a
maxTiga x y z = maxx (maxx x y) z
                where
                    maxx x y = if (x > y) then x else y



quicksort :: (Ord a) => [a] -> [a]    
quicksort [] = []    
quicksort (x:xs) =     
    let smallerSorted = quicksort (filter (<=x) xs)  
        biggerSorted = quicksort (filter (>x) xs)   
    in  smallerSorted ++ [x] ++ biggerSorted 

quicksort' :: (Ord a) => [a] -> [a]    
quicksort' [] = []    
quicksort' (x:xs) = smallerSorted ++ [x] ++ biggerSorted  
    where  smallerSorted = quicksort (filter (<=x) xs)  
           biggerSorted = quicksort (filter (>x) xs)   
           
-- below is bubble sort
bubbleSort :: Ord a => [a] -> [a]
bubbleSort ts = foldl swapTill [] ts

swapTill [] x = [x]
swapTill (y:xs) x = max x y : swapTill xs (min x y)


data Direction = North | East | South | West deriving (Eq,Show,Enum)
right :: Direction -> Direction
right d = toEnum (succ (fromEnum d) `mod` 4)

-- Direction adalah type data yang derived type Eq, Show, dan Enum
-- tiap arah ini akan memiliki nilai Int dari Enum-nya
-- misal jika kita menjalankan 'fromEnum North', akan mengembalikan nilai 0, East adalah 1 dst.
-- jadi cara berjalan program right kira kira begini:
-- sebuah arah akan diambil nilai Enum-nya, diambil succ nya (North dari 0 jadi 1 dst)
-- kemudian di `mod` 4 (karena Direction ada 4) dan dikembalikan menjadikan Direction
-- right North akan menghasilkan East, right East menjadi South dst

data Expr = C Float | Expr :+ Expr | Expr :- Expr
    | Expr :* Expr | Expr :/ Expr
    | V String
    | Let String Expr Expr
        deriving Show


data Tree a = Leaf a | Branch (Tree a) (Tree a)
    deriving Show

mapTree :: (a->b) -> Tree a -> Tree b

mapTree f (Leaf x) = Leaf (f x)
mapTree f (Branch t1 t2) = Branch (mapTree f t1) (mapTree f t2)

-- foldtl f z (Leaf x) = f z x
-- foldtl f z (Branch t1 t2) = foldtl f (f z t1) t2

-- foldtr f z (Leaf x) = f z x
-- foldtr f z (Branch t1 t2) = f t1 (foldtr f z t2)


bubbleSort :: Ord a => [a] -> [a]
bubbleSort ts = foldl swapTill [] ts

swapTill [] x = [x]
swapTill (y:xs) x = max x y : swapTill xs (min x y)


-- evaluate way
-- bubbleSort [3,6,4]
-- foldl swapTill [] [3,6,4]
--              let hasil = swapTill [] 3
--              in foldl swapTill hasil [6,4]
--          nanti jadi foldl swapTill [3] [6,4]
-- swapTill [3] 6
-- swapTill (3:[]) 6 = max 6 3 : swapTill [] 3
-- 6 ++ 3
-- [6,3]

-- foldl swapTill [6,3] [4]
-- swapTill [6,3] 4
-- swapTill (6:3) 4 = 6 : swapTill [3] 4
-- swapTill (3:[]) 4 = 4 : swapTill [] 3
--  4 ++ 3
-- [4,3]
-- 6 ++ [4,3]
-- [6,4,3]