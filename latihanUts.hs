--a function that takes list of Number and make a list of tuple
-- that contains each element and the power of it
-- newMap :: (Num a) => [a] -> ([a] -> (a,a)) -> [(a,a)]
newMap :: (Integral a) => [a] -> [(a,a)]
newMap [] = []
newMap (x:xs) = (x,x*x) : newMap xs


--KPK
kpk x y = [z | z <- [x,x+x..], z `mod` y == 0] !! 0

--FPB
gcd' :: Integer -> Integer -> Integer
gcd' a b
    | b == 0 = abs a
    | otherwise = gcd' b (a `mod` b)

primas = sieve [2..]
    where sieve (x:xs) = x : sieve [y | y<-xs, y `mod` x /=0]

factorial x = foldr (*) 1 [1..x]

permutation n k = (factorial n) `div` (factorial (n-k))

-- maxList a = foldl (max) []
pythagoras :: (Eq a,Integral a ) => [(a,a,a)]
pythagoras = [(x,y,z) | z<-[5..], y<-[4..z], x<-[3..y], x^2+y^2==z^2]