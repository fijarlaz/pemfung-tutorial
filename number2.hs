import Data.Maybe
-- 1.
sumx :: Num a => [a] -> [a]
sumx x = map (+1) x


-- 2.
sumxy :: Num a => [a] -> [a] -> [a]
sumxy xs ys = foldr (++) [] (map (\x -> map (\y -> x + y) ys) xs)

-- 3.
sumx2 :: (Num a, Ord a) => [a] -> [a]
sumx2 xs = map (+2) $ (filter (>3) xs)

-- 4.
sumTup :: Num a => [(a,b)] -> [a]
-- tess xys = 
sumTup xys = map (+3) $ foldr (++) [] $ sing $ map (fst) xys
                where sing xs = [xs]


test1 xys = [ x+3 | (x,_) <- xys ]
a = [(1,2),(3,4),(1,3)]
-- 5.
examp xys = [ x+4 | (x,y) <- xys, x+y < 5 ]
-- tamTup :: (Num a, Ord a) => [(a,a)] -> a
-- tamTup xys = map (+4) $ foldr (++) [] $ map (fst) xys
-- tamTup xys = map (+4) $ filter (\x -> map <5) $  (map (\xy -> fst xys + snd xys) xys)
tamTup xys =  map (+4) $ filter (<5) $ map (\xys -> fst xys + snd xys) xys
-- map (+4) dari fst tupp
-- tupp adalah list dari fst tuple input
-- tuple harus diiterasi dan difilter
-- filter (<5) fst xys + snd xys
-- jika memenuhi, ambil fst xys
-- berarti fungsi pertama adalah filter, filter (<5) $ map (\xys -> fst xys + snd xys) xys -> ini menghasilkan list yang berisi penjumlahan tuple
-- jika memenuhi filter di atas, maka diambil fst nya saja
-- pakai let?
-- let smallTup = filter (<5) $ map (\xys -> fst xys + snd xys) xys
-- jujur pak saya bingung yang ini

-- 6.
lis = [Just 1, Just 2, Just 3]
contoh mxs = [ x+5 | Just x <- mxs ]
-- saya tidak tahu cara lain selain menggunakan module Data.Maybe
numSix mxs = map (fromJust) $ fmap (+5) <$> mxs

-- change map (+3) xs
llst = [1,2,3,4,8,9]
mapToList xs = [x+3 | x <- xs]

-- change filter (>7) xs

filToList xs = [x | x <- xs, x>7]

-- change concat (map (\x -> map (\y -> (x,y)) ys) xs)
conEx xs ys = concat (map (\x -> map (\y -> (x,y)) ys) xs)

conToList xs ys = [(x,y) | x <- xs, y <- ys]

-- filter (>3) (map (\(x,y) -> x+y) xys)
filEx xys = filter (>3) (map (\(x,y) -> x+y) xys)

filMapLst xys = [x+y | (x,y) <- xys, x+y > 3]